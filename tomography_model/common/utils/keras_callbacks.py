import tensorflow as tf
# from Constants import MODEL_CHECKPOINT_FILE

# MODEL_CHECKPOINT = tf.keras.callbacks.ModelCheckpoint(
#     filepath=MODEL_CHECKPOINT_FILE
# )

STOP_IF_NOT_IMPROVING = tf.keras.callbacks.EarlyStopping(
    patience=10,
    restore_best_weights=False
)

AUTO_REDUCE_LR_IF_NOT_IMPROVING = tf.keras.callbacks.ReduceLROnPlateau(
    patience=10
)

STOP_ON_NAN_IN_LOSS = tf.keras.callbacks.TerminateOnNaN()
