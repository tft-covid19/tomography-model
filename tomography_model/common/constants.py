import tensorflow as tf
from .utils.keras_callbacks import STOP_IF_NOT_IMPROVING, AUTO_REDUCE_LR_IF_NOT_IMPROVING, STOP_ON_NAN_IN_LOSS

# PATHS
REPO_BASE_URL = '/content/gdrive/MyDrive/ULPGC/TFT/projects/Tomography-Model/'
PROJECT_BASE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/'

# DATASETS
DATASET_2 = {'path': '/content/gdrive/MyDrive/ULPGC/TFT/data/dataset-2/COVID-CT-master/Images-processed/',
             'name': 'Dataset 2',
             'classes': ['CT_COVID', 'CT_NonCOVID']}

AUGMENTED_DATASET_2 = {
    'path': '{}data/dataset-2/bright-contrast-augmented-dataset+/'.format(PROJECT_BASE_PATH),
    'name': 'Augmented dataset 2',
    'classes': ['CT_COVID', 'CT_NonCOVID']
}

# IMAGES
IMAGE_SIZE = 200
TRAIN_IMAGES_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_2/train_images'
TRAIN_LABELS_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_2/train_labels'
TEST_IMAGES_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_2/test_images'
TEST_LABELS_FILE_PATH = '/content/gdrive/MyDrive/ULPGC/TFT/data/data_variable_files/dataset_2/test_labels'

# MODEL PARAMETERS
CONV_WINDOW_SIZE = (5, 5)
POOLING_SIZE = (2, 2)
IMAGES_SHAPE = (200, 200, 3)
EPOCHS = 60
PADDING = 'same'
POOLING_PADDING = 'same'
POOLING_STRIDE = (2, 2)
BATCH_SIZE = 50
OPTIMIZER = 'Adam'
LOSS = tf.keras.losses.BinaryCrossentropy()
VALIDATION_SPLIT = 0.2
CONV_ACTIVATION = 'relu'
WEIGHT_INITILIZER = tf.keras.initializers.GlorotNormal()
CALLBACKS = [STOP_IF_NOT_IMPROVING,
             AUTO_REDUCE_LR_IF_NOT_IMPROVING,
             STOP_ON_NAN_IN_LOSS]

# OTHER
LOG_PATH = '{}Tomography-Model/log/Dataset_2_tests.csv'.format(REPO_BASE_URL)
